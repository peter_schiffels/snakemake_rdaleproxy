# Snakemake - a pythonic workflow system

Snakemake is a workflow management system that aims to reduce the complexity of creating workflows 
by providing a fast and comfortable execution environment, together with a clean and readable 
specification language in Python style. Snakemake workflows are essentially Python scripts extended 
by declarative code to define rules. Rules describe how to create output files from input files.

Homepage: http://snakemake.bitbucket.org

Copyright (c) 2012-2017 Johannes K�ster <johannes.koester@protonmail.com> (see LICENSE)
